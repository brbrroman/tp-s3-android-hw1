package com.example.tp_s3_android_hw1;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

public final class MainActivity extends AppCompatActivity implements Fragment1.OnItemSelectListener{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null) {
            Fragment1 firstFrag = new Fragment1();

            final FragmentTransaction tr1 = getSupportFragmentManager().beginTransaction();
            tr1.replace(R.id.container, firstFrag);
            tr1.commit();
        }
    }

    @Override
    public void onItemClicked(int number, int numColor) {
        final Bundle bundle = new Bundle();
        bundle.putInt(Fragment2.NUMBER_KEY, number);
        bundle.putInt(Fragment2.COLOR_KEY, numColor);
        Fragment2 frag2 = new Fragment2();
        frag2.setArguments(bundle);

        FragmentTransaction tr1 = getSupportFragmentManager().beginTransaction();
        tr1.replace(R.id.container, frag2);
        tr1.addToBackStack(null);
        tr1.commit();

    }
}
