package com.example.tp_s3_android_hw1;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class Fragment1 extends Fragment {
    private static final String ITEMS_KEY = "Fragment1.items";

    private MyAdapter adapter;
    private OnItemSelectListener listener;
    private List<Integer> items;

    private Button myButton;

    public interface OnItemSelectListener {
        void onItemClicked(int number, int numColor);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (OnItemSelectListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        int cols = 1;
        FragmentActivity activity = getActivity();
        Resources res = getResources();
        int maxColsPortrait = res.getInteger(R.integer.max_cols_portrait);
        int maxColsLandscape = res.getInteger(R.integer.max_cols_landscape);
        if (activity != null) {
            final int orientation = activity.getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                cols = maxColsPortrait;
            } else {
                cols = maxColsLandscape;
            }
        }
        View view = inflater.inflate(R.layout.fragment1, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recentrecyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new GridLayoutManager(this.getContext(), cols);
        recyclerView.setLayoutManager(layoutManager);
        this.adapter = new MyAdapter(this.items, this.listener);
        recyclerView.setAdapter(this.adapter);

        this.myButton = view.findViewById(R.id.buttonAdd);
        this.myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.addNum();
                adapter.notifyDataSetChanged();
            }
            });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.myButton.setOnClickListener(null);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            initData();
        } else {
            this.items = savedInstanceState.getIntegerArrayList(ITEMS_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(@Nullable Bundle outState) {
        if(outState == null)
            outState = new Bundle();
        outState.putIntegerArrayList(ITEMS_KEY, (ArrayList<Integer>) this.items);
        super.onSaveInstanceState(outState);
    }

    private void initData() {
        this.items = new ArrayList<>();
        final Random random = new Random();
        for (int i = 1; i < random.nextInt(100 + 1); ++i) {
            this.items.add(i);
        }
    }
}