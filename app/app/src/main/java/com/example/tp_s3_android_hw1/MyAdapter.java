package com.example.tp_s3_android_hw1;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public final class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private final List<Integer> items;
    private final Fragment1.OnItemSelectListener listener;

    public MyAdapter(@NonNull List<Integer> items, @NonNull  Fragment1.OnItemSelectListener listener) {
        this.items = items;
        this.listener = listener;
    }

    public void addNum() {
        this.items.add((this.items.size() + 1));
    }

    public final class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView viewHolderNumber;

        MyViewHolder(@NonNull View view) {
            super(view);
            this.viewHolderNumber = view.findViewById(R.id.numberTextView);
            this.viewHolderNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int number = items.get(getAdapterPosition());
                    final boolean even = number % 2 == 0;
                    listener.onItemClicked(number, even ? Color.RED : Color.BLUE);
                }
            });
        }
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                     int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.number_layout, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final int number = this.items.get(position);
        final boolean even = number % 2 == 0;
        holder.viewHolderNumber.setText(String.valueOf(number));
        holder.viewHolderNumber.setTextColor(even ? Color.RED : Color.BLUE);
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }
}
