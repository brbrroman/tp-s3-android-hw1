package com.example.tp_s3_android_hw1;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public final class Fragment2 extends Fragment {
    private Integer number = 0;
    private Integer numColor = 0;
    public static final String NUMBER_KEY = "Fragment2.number";
    public static final String COLOR_KEY = "Fragment2.color";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            this.number = args.getInt(NUMBER_KEY, 0);
            this.numColor= args.getInt(COLOR_KEY, 0);
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment2, container, false);
        final TextView textNum = view.findViewById(R.id.fragment2_textView);
        textNum.setText(String.valueOf(this.number));
        textNum.setTextColor(this.numColor);
        return view;
    }
}
